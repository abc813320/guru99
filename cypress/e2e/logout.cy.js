describe('Guru99 Logout', () => {
    it('Should login and then logout successfully', () => {
        // Visit the login page
        cy.visit('https://demo.guru99.com/test/newtours/login.php');

        // Fill out the login form
        cy.get('input[name="userName"]').type('example@gmail.com');
        cy.get('input[name="password"]').type('your_password');

        // Submit the login form
        cy.get('input[name="submit"]').click();

        // Verify successful login
        // Assuming there's a specific element visible only when logged in, like a logout button or a specific text
       // cy.contains('Login Successfully').should('be.visible');

        // Perform the logout
        cy.contains('SIGN-OFF').click();  // This selector depends on the actual logout element

        // Verify successful logout
        
    });
});
