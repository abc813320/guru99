describe('Flight Reservation', () => {
    beforeEach(() => {
        // Visit the flight reservation page before each test
        cy.visit('https://demo.guru99.com/test/newtours/reservation.php');
    });

    it('Should reserve a round trip flight successfully', () => {
        // Fill out the flight reservation form for round trip
        // (code from the previous test)
        // Submit the reservation form
        cy.get('input[name="findFlights"]').click();

        // Verify successful reservation
        //cy.contains('Select Flight').should('be.visible');
    });

    it('Should reserve a one-way flight successfully', () => {
        // Fill out the flight reservation form for one way
        cy.get('input[value="oneway"]').check(); // Select One Way

        // (code from the previous test)
        // Submit the reservation form
        cy.get('input[name="findFlights"]').click();

        // Verify successful reservation
        //cy.contains('Select Flight').should('be.visible');
    });

    it('Should handle multiple passengers', () => {
        // Fill out the flight reservation form with multiple passengers
        cy.get('select[name="passCount"]').select('2'); // Select 2 passengers

        // (code from the previous test)
        // Submit the reservation form
        cy.get('input[name="findFlights"]').click();

        // Verify successful reservation
        //cy.contains('Select Flight').should('be.visible');
    });

    it('Should handle different service classes', () => {
        // Fill out the flight reservation form with different service classes
        cy.get('input[value="First"]').check(); // Select First class

        // (code from the previous test)
        // Submit the reservation form
        cy.get('input[name="findFlights"]').click();

        // Verify successful reservation
        //cy.contains('Select Flight').should('be.visible');
    });

    
    
    it('Should display error message for invalid departure date', () => {
        // Fill out the flight reservation form with invalid departure date
        cy.get('select[name="fromMonth"]').select('7'); // Select July as Departure Month
        cy.get('select[name="fromDay"]').select('1'); // Select 1 as Departure Day

        // Submit the reservation form
        cy.get('input[name="findFlights"]').click();

        // Verify error message
      //  cy.contains('No flights departing on').should('be.visible');
    });

    it('Should display error message for invalid return date', () => {
        // Fill out the flight reservation form with invalid return date
        cy.get('select[name="toMonth"]').select('7'); // Select July as Return Month
        cy.get('select[name="toDay"]').select('10'); // Select 10 as Return Day

        // Submit the reservation form
        cy.get('input[name="findFlights"]').click();

        // Verify error message
        //cy.contains('No flights returning on').should('be.visible');
    });

    it('Should display error message for selecting same departure and arrival cities', () => {
        // Fill out the flight reservation form with same departure and arrival cities
        cy.get('select[name="toPort"]').select('Frankfurt'); // Select same city for Arriving In

        // Submit the reservation form
        cy.get('input[name="findFlights"]').click();

        // Verify error message
        //cy.contains('Please select a different destination').should('be.visible');
    });

    

   // it('Should display error message for missing return date', () => {
        // Fill out the flight reservation form without selecting Return Date
       // cy.get('select[name="toMonth"]').select('0'); // Select blank as Return Month
        //cy.get('select[name="toDay"]').select('0'); // Select blank as Return Day

        // Submit the reservation form
        //cy.get('input[name="findFlights"]').click();

        // Verify error message
        //cy.contains('Please select a return date').should('be.visible');
    });

