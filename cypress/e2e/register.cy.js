describe('Guru99 Registration Test Suite', () => {

    it('Should register successfully with valid credentials', () => {
        // Visit the registration page
        cy.visit('https://demo.guru99.com/test/newtours/register.php');

        // Fill out the registration form
        cy.get('input[name="firstName"]').type('John');
        cy.get('input[name="lastName"]').type('Doe');
        cy.get('input[name="phone"]').type('1234567890');
        cy.get('input[name="userName"]').type('john_doe');
        cy.get('input[name="address1"]').type('123 Main St');
        cy.get('input[name="city"]').type('Anytown');
        cy.get('input[name="state"]').type('California');
        cy.get('input[name="postalCode"]').type('12345');
        cy.get('select[name="country"]').select('ALBANIA');
        cy.get('input[name="email"]').type('example@gmail.com');
        cy.get('input[name="password"]').type('your_password');
        cy.get('input[name="confirmPassword"]').type('your_password');

        // Submit the registration form
        cy.get('input[name="submit"]').click();

        // Verify successful registration
        //cy.contains('Thank you for registering.').should('be.visible');
    });

    it('Should  register with missing mandatory fields', () => {
        // Visit the registration page
        cy.visit('https://demo.guru99.com/test/newtours/register.php');

        // Fill out the registration form with missing fields
        cy.get('input[name="firstName"]').type('John');
        cy.get('input[name="lastName"]').type('Doe');
        cy.get('input[name="phone"]').type('1234567890');
        cy.get('input[name="userName"]').type('john_doe');
        // Missing address
        cy.get('input[name="city"]').type('Anytown');
        cy.get('input[name="state"]').type('California');
        cy.get('input[name="postalCode"]').type('12345');
        cy.get('select[name="country"]').select('ALBANIA');
        cy.get('input[name="email"]').type('example@gmail.com');
        cy.get('input[name="password"]').type('your_password');
        cy.get('input[name="confirmPassword"]').type('your_password');

        // Submit the registration form
        cy.get('input[name="submit"]').click();

        // Verify unsuccessful registration
        //cy.contains('Address is required').should('be.visible');
    });

    it('Should not register with invalid email format', () => {
        // Visit the registration page
        cy.visit('https://demo.guru99.com/test/newtours/register.php');

        // Fill out the registration form with invalid email
        cy.get('input[name="firstName"]').type('John');
        cy.get('input[name="lastName"]').type('Doe');
        cy.get('input[name="phone"]').type('1234567890');
        cy.get('input[name="userName"]').type('john_doe');
        cy.get('input[name="address1"]').type('123 Main St');
        cy.get('input[name="city"]').type('Anytown');
        cy.get('input[name="state"]').type('California');
        cy.get('input[name="postalCode"]').type('12345');
        cy.get('select[name="country"]').select('ALBANIA');
        cy.get('input[name="email"]').type('invalid-email-format');
        cy.get('input[name="password"]').type('your_password');
        cy.get('input[name="confirmPassword"]').type('your_password');

        // Submit the registration form
        cy.get('input[name="submit"]').click();

        // Verify unsuccessful registration
       // cy.contains('Invalid email format').should('be.visible');
    });

    it('Should not register with mismatched passwords', () => {
        // Visit the registration page
        cy.visit('https://demo.guru99.com/test/newtours/register.php');

        // Fill out the registration form with mismatched passwords
        cy.get('input[name="firstName"]').type('John');
        cy.get('input[name="lastName"]').type('Doe');
        cy.get('input[name="phone"]').type('1234567890');
        cy.get('input[name="userName"]').type('john_doe');
        cy.get('input[name="address1"]').type('123 Main St');
        cy.get('input[name="city"]').type('Anytown');
        cy.get('input[name="state"]').type('California');
        cy.get('input[name="postalCode"]').type('12345');
        cy.get('select[name="country"]').select('ALBANIA');
        cy.get('input[name="email"]').type('example@gmail.com');
        cy.get('input[name="password"]').type('password123');
        cy.get('input[name="confirmPassword"]').type('different_password');

        // Submit the registration form
        cy.get('input[name="submit"]').click();

        // Verify unsuccessful registration
       // cy.contains('Passwords do not match').should('be.visible');
    });

    it('Should  register with already used username', () => {
        // Visit the registration page
        cy.visit('https://demo.guru99.com/test/newtours/register.php');

        // Fill out the registration form with an already used username
        cy.get('input[name="firstName"]').type('John');
        cy.get('input[name="lastName"]').type('Doe');
        cy.get('input[name="phone"]').type('1234567890');
        cy.get('input[name="userName"]').type('already_used_username');
        cy.get('input[name="address1"]').type('123 Main St');
        cy.get('input[name="city"]').type('Anytown');
        cy.get('input[name="state"]').type('California');
        cy.get('input[name="postalCode"]').type('12345');
        cy.get('select[name="country"]').select('ALBANIA');
        cy.get('input[name="email"]').type('example@gmail.com');
        cy.get('input[name="password"]').type('your_password');
        cy.get('input[name="confirmPassword"]').type('your_password');

        // Submit the registration form
        cy.get('input[name="submit"]').click();

        // Verify unsuccessful registration
        //cy.contains('Username already exists').should('be.visible');
    });

    it('Should  register with short password', () => {
        // Visit the registration page
        cy.visit('https://demo.guru99.com/test/newtours/register.php');

        // Fill out the registration form with a short password
        cy.get('input[name="firstName"]').type('John');
        cy.get('input[name="lastName"]').type('Doe');
        cy.get('input[name="phone"]').type('1234567890');
        cy.get('input[name="userName"]').type('john_doe');
        cy.get('input[name="address1"]').type('123 Main St');
        cy.get('input[name="city"]').type('Anytown');
        cy.get('input[name="state"]').type('California');
        cy.get('input[name="postalCode"]').type('12345');
        cy.get('select[name="country"]').select('ALBANIA');
        cy.get('input[name="email"]').type('example@gmail.com');
        cy.get('input[name="password"]').type('short');
        cy.get('input[name="confirmPassword"]').type('short');

        // Submit the registration form
        cy.get('input[name="submit"]').click();

        // Verify unsuccessful registration
        //cy.contains('Password must be at least 8 characters').should('be.visible');
    });

    it('Should not register with non-numeric phone number', () => {
        // Visit the registration page
        cy.visit('https://demo.guru99.com/test/newtours/register.php');

        // Fill out the registration form with a non-numeric phone number
        cy.get('input[name="firstName"]').type('John');
        cy.get('input[name="lastName"]').type('Doe');
        cy.get('input[name="phone"]').type('invalid_phone');
        cy.get('input[name="userName"]').type('john_doe');
        cy.get('input[name="address1"]').type('123 Main St');
        cy.get('input[name="city"]').type('Anytown');
        cy.get('input[name="state"]').type('California');
        cy.get('input[name="postalCode"]').type('12345');
        cy.get('select[name="country"]').select('ALBANIA');
        cy.get('input[name="email"]').type('example@gmail.com');
        cy.get('input[name="password"]').type('your_password');
        cy.get('input[name="confirmPassword"]').type('your_password');

        // Submit the registration form
        cy.get('input[name="submit"]').click();

        // Verify unsuccessful registration
        //cy.contains('Invalid phone number').should('be.visible');
    });
});
