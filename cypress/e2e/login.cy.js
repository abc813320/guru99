describe('Guru99 Test Suite', () => {

    it('Should login successfully with valid credentials', () => {
        // Visit the login page
        cy.visit('https://demo.guru99.com/test/newtours/login.php');

        // Fill out the login form
        cy.get('input[name="userName"]').type('example@gmail.com');
        cy.get('input[name="password"]').type('your_password');

        // Submit the login form
        cy.get('input[name="submit"]').click();

        // Verify successful login
        cy.contains('Login Successfully').should('be.visible');
    });

    it('Should not login with invalid credentials', () => {
        // Visit the login page
        cy.visit('https://demo.guru99.com/test/newtours/login.php');

        // Fill out the login form with invalid credentials
        cy.get('input[name="userName"]').type('invalid_user');
        cy.get('input[name="password"]').type('invalid_password');

        // Submit the login form
        cy.get('input[name="submit"]').click();

        // Verify unsuccessful login
        cy.contains('Login Successfully').should('not.exist');
        cy.contains('Enter your userName and password correct').should('be.visible');
    });

    it('Should navigate to the Flights page after login', () => {
        // Visit the login page
        cy.visit('https://demo.guru99.com/test/newtours/login.php');

        // Fill out the login form
        cy.get('input[name="userName"]').type('example@gmail.com');
        cy.get('input[name="password"]').type('your_password');

        // Submit the login form
        cy.get('input[name="submit"]').click();

        // Verify successful login
        cy.contains('Login Successfully').should('be.visible');
        cy.contains('Flights').click();

        // Verify navigation to the Flights page
        cy.url().should('include', '/reservation.php');
        cy.contains('Flight Details').should('be.visible');

    });
    it('Should login successfully with null email', () => {
        // Visit the login page
        cy.visit('https://demo.guru99.com/test/newtours/login.php');

        // Fill out the login form
       // cy.get('input[name="userName"]').type('example@gmail.com');
        cy.get('input[name="password"]').type('your_password');

        // Submit the login form
        cy.get('input[name="submit"]').click();

        // Verify successful login
        //cy.contains('Login Successfully').should('be.visible');
    });
    it('Should\'t login successfully with null email', () => {
        // Visit the login page
        cy.visit('https://demo.guru99.com/test/newtours/login.php');

        // Fill out the login form
       // cy.get('input[name="userName"]').type('example@gmail.com');
       cy.get('input[name="userName"]').type('example@gmail.com');

       

        // Submit the login form
        cy.get('input[name="submit"]').click();

        // Verify successful login
        cy.contains('Enter your userName and password correct').should('be.visible');
    });

});